-module(primes_sprimary).

-behaviour(supervisor).

-export([start_link/0, init/1]).

-define(DEFAULT_TIMEOUT, 3600).


%%
%% API functions
%%

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).


%%
%% Supervisor callbacks
%%

init([]) ->
    {ok, { {one_for_one, 5, 10}, 
           [
            child_spec(primes_primary, primes_primary, worker)
           ]} }.

child_spec(Name, M, Type) ->
    StartFunc = {M, start_link, []},
    Restart = permanent,
    Shutdown = ?DEFAULT_TIMEOUT,
    Modules = [M],
    {Name, StartFunc, Restart, Shutdown, Type, Modules}.
