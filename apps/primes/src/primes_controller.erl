-module(primes_controller).

-export([start_link/0]). 
-export([init/1, primes/2]).
-export([range/3, resend_primes/2]).

-define(DEFAULT_TIMEOUT, 10000).
-define(DEFAULT_RESP_TIMEOUT, 2000).
-define(NWORKERS, 10).

start_link() ->
    Args = [self()],
    proc_lib:start_link(?MODULE, init, Args).

init(Parent) ->
    ets:new(table, [set, named_table]),
    ets:insert(table, {n_received, 0}),
    Names = lists:map(fun process_name/1, lists:seq(1, ?NWORKERS)),
    lists:foreach(fun (Name) -> ets:insert(table, {Name, []}) end, Names),
    proc_lib:init_ack(Parent, {ok, self()}),
    register(?MODULE, self()),
    loop({[],[],[]}, false).

primes(A, B) ->
    ?MODULE ! {self(), {primes, A, B}},
    receive
        {_From, {primes_result, Result}} ->
            Result;
        X ->
            X
    after ?DEFAULT_TIMEOUT ->
            error
    end.

loop(State, Busy) ->
    {Parent, _, _} = State,
%    error_logger:info_msg("busy ~p~n", [Busy]),
    receive
        {From, {primes, A, B}} ->
%            error_logger:info_msg("message primes~n"),
            if Busy == true ->
                    From ! {self(), busy},
                    loop(State, true);
               true ->          
                    %ensure_workers(?NWORKERS),
                    proc_primes(A, B),
                    loop({From, A, B}, true)
            end;
        
        {_From, FromName, {primes_result, Result}} ->
%            error_logger:info_msg("primes result ~p - ~p~n", [FromName, Result]),
            save_result(FromName, Result),
            error_logger:info_msg("all done ~p~n", [all_done()]),
            case all_done() of
                true ->
                    Parent ! {self(), {primes_result, results()}},
                    cleanTable(),
                    loop({[],[],[]}, false);
                _ ->
                    loop(State, true)
            end;

        {From, {stop}} ->
            From ! {self(), {ok}};

        {From, Other} ->
            error_logger:info_msg("unsupported message ~p~n", [Other]),
            From ! {self(), {error, Other}};

        Other ->
            error_logger:info_msg("unsupported message ~p~n", [Other])

    after ?DEFAULT_RESP_TIMEOUT ->
            if Busy == true ->
                    error_logger:info_msg("child was killed!!!~n", []),
                    {_, A, B} = State,
                    resend_primes(A, B),
                    loop(State, Busy);
               true ->
                    loop(State, Busy)
            end
    end.

resend_primes(A, B) ->
    Names = lists:map(fun process_name/1, lists:seq(1, ?NWORKERS)),
    NNames = lists:zip(Names, lists:seq(1, ?NWORKERS)),
    FNames = lists:filter(fun ({Name, _I}) ->
                                   [{_Name, Value}|_] = ets:lookup(table, Name),
                                   Value == []
                          end, NNames),
    lists:foreach(fun ({Name, I}) ->
                          Range = range(I, A, B),
                          {Pid, _} = gproc:await({n, l, Name}, 3000),
                          Pid ! {self(), {primes, Range}}
                  end, FNames),
    FNames.

proc_primes(From, To) ->
    Names = lists:map(fun process_name/1, lists:seq(1, ?NWORKERS)),
    Ranges = lists:map(fun (I) -> range(I, From, To) end, lists:seq(1, ?NWORKERS)),
    lists:foreach(fun ({Name, Range}) ->
                          %error_logger:info_msg("awaiting ~p~n", [Name]),
                          {Pid, _} = gproc:await({n, l, Name}, 3000),
                          %error_logger:info_msg("sending to ~p~n", [Pid]),
                          Pid ! {self(), {primes, Range}}
                  end, lists:zip(Names, Ranges)).

range(I, From, To) ->
    PerProcess = (To - From) div ?NWORKERS,
    case I == ?NWORKERS of
        true ->
            {(From + (I - 1) * PerProcess), To};
        _ ->
            {(From + (I - 1) * PerProcess), (From + I * PerProcess) - 1}
    end.

cleanTable() ->
    Names = lists:map(fun process_name/1, lists:seq(1, ?NWORKERS)),
    lists:foreach(fun (Name) -> 
                          ets:update_element(table, Name, {2, []})
                  end, Names),
    ets:update_element(table, n_received, {2, 0}).

all_done() ->
    [{n_received, N}|_] = ets:lookup(table, n_received),
    N == ?NWORKERS.

results() ->
    lists:concat(
      ets:select(table, [{ {'$1','$2'}, [{'/=', '$1', n_received}], ['$2'] }])
    ).

save_result(From, Result) ->
    ets:update_element(table, From, {2, Result}),
    [{n_received, N}|_] = ets:lookup(table, n_received),
    ets:update_element(table, n_received, {2, N + 1}).

ensure_workers(N) ->
%    error_logger:info_msg("ensuring workers~n"),
    case lists:keyfind(workers, 1, supervisor:count_children(primes_rsup)) of
        {workers, Count} ->
            %error_logger:info_msg("children: ~p~n", [Count]),
            if 
               Count == 0 ->
                    %error_logger:info_msg("creating children~n"),
                    Names = lists:map(fun process_name/1, lists:seq(1, N)),
                    lists:foreach(fun (Name) -> ets:insert(table, {Name, []}) end, Names),
                    lists:map(fun start_child/1, Names);
                true -> ok
            end;
        false -> erlang:error({noSpecsProperty})
    end.

process_name(N) when is_integer(N) ->
    atom_to_list(primes_routine) ++ "_" ++ integer_to_list(N).

start_child(Name) ->
    supervisor:start_child(sup(), [Name]).

sup() ->
    gproc:lookup_local_name(primes_rsup).
