-module(primes_rsup).
-behaviour(supervisor).

-export([start_link/0, start_in_shell_for_testing/0]).
-export([init/1]).
-export([child_spec/1]).
-export([children/0]).


-define(DEFAULT_TIMEOUT, 3600).
-define(NWORKERS, 10).

%%
%% API functions
%%

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_in_shell_for_testing() ->
    {ok, Pid} = supervisor:start_link(primes_rsup, []),
    unlink(Pid),
    Pid.

%%
%% Supervisor callbacks
%%

init(_Args) ->
    gproc:add_local_name(primes_rsup),
    {ok, {{one_for_one, 10, 60},
          children()}}.

children() ->
    Names = lists:map(fun process_name/1, lists:seq(1, ?NWORKERS)),
    error_logger:info_msg("Names: ~p~n", [Names]),
    lists:map(fun child_spec/1, Names).

process_name(N) when is_integer(N) ->
    atom_to_list(primes_routine) ++ "_" ++ integer_to_list(N).

child_spec(Name) ->
    StartFunc = {primes_routine, start_link, [Name]},
    Restart = permanent,
    Shutdown = ?DEFAULT_TIMEOUT,
    Modules = [primes_routine],
    Type = worker,
    {Name, StartFunc, Restart, Shutdown, Type, Modules}.
