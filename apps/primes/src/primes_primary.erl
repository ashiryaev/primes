-module(primes_primary).

-export([start_link/0]). 
-export([init/1, primes/2]).
-export([safe_zip/2, save_result/1]).

%-define(DEFAULT_TIMEOUT, 10000).
%-define(DEFAULT_RESP_TIMEOUT, 2000).
%-define(NWORKERS, 10).

start_link() ->
    Args = [self()],
    proc_lib:start_link(?MODULE, init, Args).


init(Parent) ->
    ets:new(table, [set, named_table]),
    init_table(),
    proc_lib:init_ack(Parent, {ok, self()}),
    register(?MODULE, self()),
    loop({[], [], []}, false).

%OK
save_result(Result) ->
    File = "/tmp/testfile.txt",
    {ok, S} = file:open(File, write),
    lists:foreach(fun(X) -> io:format(S, "~p," ,[X]) end, Result),
    file:close(S).

loop(State, Busy) ->
    {Parent, Aa, Bb} = State,
    error_logger:info_msg("primary loop busy ~p~n", [Busy]),
    receive
        {From, {primes, A, B}} ->
            error_logger:info_msg("primary primes~n", []),
            if Busy == true ->
                    From ! {self(), busy},
                    loop(State, true);
               true ->
                    proc_primes(A, B),
                    loop({From, A, B}, true)
            end;

        {_From, FromNode, {primes_result, Result}} ->
            error_logger:info_msg("primary result from node ~p~n", [FromNode]),
            save_result(Result),
            resend_primes(Aa, Bb),
            case all_done() of
                true ->
                    Parent ! {self(), {primes, all_done}},
                    init_table(),
                    loop({[],[],[]}, false);
                _ ->
                    loop(State, true)
            end;

        {From, {stop}} ->
            From ! {self(), {ok}};

        {From, Other} ->
            error_logger:info_msg("primary unsupported message ~p~n", [Other]),
            From ! {self(), {error, Other}};
        
        % node is down
        {'DOWN', _MonitorRef, _Type, Object, _Info} ->
            {_RegName, Node} = Object, 
            failed_node(Node),
            resend_primes(Aa, Bb),
            loop(State, Busy);

        Other ->
            error_logger:info_msg("primary unsupported message ~p~n", [Other])
    end.

init_table() ->
    ets:delete_all_objects(table),
    ets:insert(table, {n_received, 0}),
    Nodes = [node() | nodes()],
    NNodes = lists:zip(
               lists:seq(1, lists:flatlength(Nodes)),
               Nodes),
    lists:foreach(fun (I, Node) ->
                          ets:insert(table, {Node, I, false}) end,
                  NNodes).    

primes(A, B) ->
    ?MODULE ! {self(), {primes, A, B}}.

failed_node(Node) ->
    ets:update_element(table, Node, {3, failed}).

resend_primes(A, B) ->
    FailedNodes = ets:select(table, [{ {'$1','$2', '$3'}, [{'==', '$3', failed}], [{{'$1', '$2'}}]}]),
    Nodes = nodes(),
    AvailableNodes = lists:filter(
          fun (Node) ->
                  Ns = ets:select(table, [{ {'$1','$2', '$3'}, [{'==', '$1', Node}], ['$_']}]),
                  case lists:flatlength(Ns) of
                      0 ->
                          true;
                      _ ->
                          [{_Node, _I, St}] = Ns,
                          case St of
                              true   -> true;
                              failed -> true;
                              _      -> false
                          end
                  end
          end,
                       Nodes),

    
    lists:foreach(fun ({{Node, I}, ANode}) ->
                          ets:delete(table, Node),
                          {primes_controller, Node} ! {primes, range(I, A, B)},
                          case ets:lookup(table, ANode) of
                              [] ->
                                  ets:insert(table, {ANode, I, false});
                              [{_Key, _I, _Value}] ->
                                  ets:update_element(table, ANode, {2, I}),
                                  ets:update_element(table, ANode, {3, false})
                          end
                  end,
                  safe_zip(FailedNodes, AvailableNodes)).

% TODO
proc_primes(From, To) ->
    Nodes = nodes(),
    Ranges = lists:map(fun (I) -> range(I, From, To) end, lists:seq(1, lists:flatlength(Nodes))),
    lists:foreach(fun ({Node, Range}) ->
                          %error_logger:info_msg("sending to ~p~n", [Node]),
                          {primes_controller, Node} ! {self(), {primes, Range}}
                  end, lists:zip(Nodes, Ranges)).

% TODO
range(I, From, To) ->
    NNodes = lists:flatlength(nodes()),
    PerProcess = (To - From) div NNodes,
    case I == NNodes of
        true ->
            {(From + (I - 1) * PerProcess), To};
        _ ->
            {(From + (I - 1) * PerProcess), (From + I * PerProcess) - 1}
    end.

% TODO
all_done() ->
    [{n_received, N}|_] = ets:lookup(table, n_received),
    N == lists:flatlength(nodes()).


%% cleanTable() ->
%%     lists:foreach(fun (Name) -> 
%%                           ets:update_element(table, Name, {2, []})
%%                   end, nodes()),
%%     ets:update_element(table, n_received, {2, 0}).


%% results() ->
%%     lists:concat(
%%       ets:select(table, [{ {'$1','$2'}, [{'/=', '$1', n_received}], ['$2'] }])
%%     ).

%% nodes() ->
%%     ets:select(table, [{ {'$1','$2'}, [{'/=', '$1', n_received}], ['$1'] }]).

%% save_result(From, Result) ->
%%     ets:update_element(table, From, {2, Result}),
%%     [{n_received, N}|_] = ets:lookup(table, n_received),
%%     ets:update_element(table, n_received, {2, N + 1}).

%% ensure_workers(N) ->
%% %    error_logger:info_msg("ensuring workers~n"),
%%     case lists:keyfind(workers, 1, supervisor:count_children(primes_rsup)) of
%%         {workers, Count} ->
%%             %error_logger:info_msg("children: ~p~n", [Count]),
%%             if 
%%                Count == 0 ->
%%                     %error_logger:info_msg("creating children~n"),
%%                     Names = lists:map(fun process_name/1, lists:seq(1, N)),
%%                     lists:foreach(fun (Name) -> ets:insert(table, {Name, []}) end, Names),
%%                     lists:map(fun start_child/1, Names);
%%                 true -> ok
%%             end;
%%         false -> erlang:error({noSpecsProperty})
%%     end.

%% process_name(N) when is_integer(N) ->
%%     atom_to_list(primes_routine) ++ "_" ++ integer_to_list(N).

%% start_child(Name) ->
%%     supervisor:start_child(sup(), [Name]).

%% sup() ->
%%     gproc:lookup_local_name(primes_rsup).

%OK
safe_zip([X | Xs], [Y | Ys]) -> [{X, Y} | safe_zip(Xs, Ys)];
safe_zip([_X | _Xs], []) -> [];
safe_zip([], [_Y | _Ys]) -> [];
safe_zip([], []) -> [].
