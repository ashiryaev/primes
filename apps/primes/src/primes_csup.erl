-module(primes_csup).
-behaviour(supervisor).

-export([start_link/0, start_in_shell_for_testing/0]).
-export([init/1]).

-define(DEFAULT_TIMEOUT, 3600).

start_link() ->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

start_in_shell_for_testing() ->
    {ok, Pid} = supervisor:start_link(primes_csup, []),
    unlink(Pid),
    Pid.

init(_Args) ->
    gproc:add_local_name(primes_csup),
    {ok, {{one_for_all, 10, 20},
          [
           child_spec(primes_controller, primes_controller, worker),
           child_spec(primes_rsup, primes_rsup, supervisor)
          ]}}.

child_spec(Name, M, Type) ->
    StartFunc = {M, start_link, []},
    Restart = permanent,
    Shutdown = ?DEFAULT_TIMEOUT,
    Modules = [M],
    {Name, StartFunc, Restart, Shutdown, Type, Modules}.
