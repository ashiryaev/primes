-module(primes_app).

-behaviour(application).

%% Application callbacks
-export([start/2, stop/1]).

%%
%% Application callbacks
%%

start(_StartType, _StartArgs) ->
    case application:get_env(primes, nodes) of
        {ok, Nodes} when is_list(Nodes) ->
            connect_nodes(Nodes);
        _ ->
            do_nothing
    end,
    
    case  application:get_env(primes, primary) of
        {ok, true} ->
            primes_sprimary:start_link(),
            primes_sup:start_link();
        _ ->
            primes_sup:start_link()
    end.

stop(_State) ->
    ok.

connect_nodes(Nodes) ->
    lists:foreach(fun(Node) ->
                          error_logger:info_msg("connecting node ~p~n", [Node]),
                          net_kernel:connect_node(list_to_atom(Node))
                  end, Nodes).
                                 
