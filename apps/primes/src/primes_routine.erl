-module(primes_routine).

-export([start_link/1, init/2]).

-include_lib("eunit/include/eunit.hrl").

%%
%% API functions
%%

start_link(Name) ->
    Args = [self(), Name],
    proc_lib:start_link(?MODULE, init, Args).

init(Parent, Name) ->
    proc_lib:init_ack(Parent, {ok, self()}),
    gproc:add_local_name(Name),
    error_logger:info_msg("routine init ~p~n", [Name]),
    loop(Name).


%%
%% Internal functions
%%

loop(Name) ->
    receive
	{From, {primes, {A, B}}} ->
	    From ! {self(), Name, {primes_result, proc_primes(A, B)}},
	    loop(Name);
	{Sender, {stop}} ->
            Sender ! {self(), Name, {ok}};
	{Sender, Other} ->
	    Sender ! {self(), Name, {error, Other}}
    end.

proc_primes(From, To) ->
    lists:filter(fun lib_primes:is_prime/1, lists:seq(From, To)).


%%
%% eunit functions
%%

primes_test() ->
    [2, 3, 5, 7, 11, 13, 17, 19] = 
        proc_primes(1, 20).
