-module(primes_routine_SUITE).

-include_lib("common_test/include/ct.hrl").

-export([all/0]).

%% Test cases
-export([primes_test/1, mod_exists/1]).

%%
%% API functions
%%

all() -> [primes_test, mod_exists].


%%
%% Test Cases
%%

primes_test(_) ->
    application:start(gproc),
    primes_routine:start_link(test),
    {Pid, Value} = gproc:await({n, l, test}),
    Pid ! {self(), {primes, {1, 20}}},
    receive
        {Pid, test, {primes_result, Result}} ->
            [2, 3, 5, 7, 11, 13, 17, 19] = Result
    after 1000 ->
            throw(timeout)
    end,
    ok.

mod_exists(_) ->
    {module, primes_routine} = code:load_file(primes_routine),
    {module, gproc} = code:load_file(gproc),
    ok.
