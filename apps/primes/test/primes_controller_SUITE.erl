-module(primes_controller_SUITE).

-include_lib("common_test/include/ct.hrl").

-export([all/0]).

%% Test cases
-export([primes_test/1, mod_exists/1, worker_failure_test/1]).

-export([proc/2]).

%%
%% API functions
%%

all() -> [mod_exists, primes_test, worker_failure_test].


%%
%% Test Cases
%%

primes_test(Config) ->
    application:start(gproc),
    primes_sup:start_link(),
    wait(500),
    Primes = lists:filter(fun lib_primes:is_prime/1, lists:seq(1, 10000)),
    Primes = lists:sort(primes_controller:primes(1, 10000)).

mod_exists(_) ->
    {module, primes_sup} = code:load_file(primes_sup),
    {module, primes_controller} = code:load_file(primes_controller),
    {module, gproc} = code:load_file(gproc).        

worker_failure_test(_) ->
    application:start(gproc),
    primes_sup:start_link(),
    wait(500),
    primes_controller:primes(1, 100),

    {Pid, Value} = gproc:await({n, l, "primes_routine_1"}, 2000),
    exit(Pid, kill),

    Worker = spawn(primes_controller_SUITE, proc, [self(), 10000]),
    error_logger:info_msg("worker pid ~p~n", [Worker]),
    {Pid2, Value} = gproc:await({n, l, "primes_routine_1"}, 2000),
    exit(Pid2, kill),
    %{Pid3, Value} = gproc:await({n, l, "primes_routine_1"}, 2000),
    %exit(Pid3, kill),
    
    receive
        {Result} ->
            Primes = lists:filter(fun lib_primes:is_prime/1, lists:seq(1, 10000)),
            Primes = lists:sort(Result)
    after (8000) ->
            throw(timeout)
    end.

proc(From, N) ->
    error_logger:info_msg("before primes calculated~n", []),
    Primes = primes_controller:primes(1, N),
    error_logger:info_msg("primes calculated ~p from ~p~n", [Primes, From]),
    From ! {Primes},
    ok.

wait(Msec) ->
    receive
    after (Msec) ->
            ok
    end.
